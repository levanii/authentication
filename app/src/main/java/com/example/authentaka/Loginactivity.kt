package com.example.authentaka

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth

class Loginactivity : AppCompatActivity() {

    private lateinit var editTextEmail: TextInputEditText
    private lateinit var editTextPassword:TextInputEditText
    private lateinit var buttonLogin:Button
    private lateinit var buttonRegistration:Button
    private lateinit var buttonPasswordReset:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(FirebaseAuth.getInstance().currentUser != null){
            goToProfile()
        }
        setContentView(R.layout.activity_login)
        init()
        registerListeners()
    }

    private fun init(){
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonLogin = findViewById(R.id.buttonLogin)
        buttonRegistration = findViewById(R.id.buttonRegistration)
        buttonPasswordReset = findViewById(R.id.buttonPasswordReset)

    }


    private fun registerListeners(){
        buttonLogin.setOnClickListener(){
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()

            if(email.isEmpty() || password.isEmpty()){
                Toast.makeText(this, "Email or Password is blank", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email,password)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        goToProfile()
                    } else {
                        Toast.makeText(this, "Registration Unsuccessful", Toast.LENGTH_SHORT).show()
                    }
                }

        }
        buttonRegistration.setOnClickListener(){
            val intent = Intent(this,RegistrationActivity::class.java)
            startActivity(intent)

        }

        buttonPasswordReset.setOnClickListener(){
            val intent = Intent(this,Loginactivity::class.java)
            startActivity(intent)

        }


    }

    private fun goToProfile(){
        startActivity(Intent(this,ProfileActivity::class.java))
        finish()
    }



}