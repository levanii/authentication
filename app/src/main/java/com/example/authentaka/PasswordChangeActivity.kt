package com.example.authentaka

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth

class PasswordChangeActivity : AppCompatActivity() {

    private lateinit var editTextPassword: TextInputEditText
    private lateinit var buttonPasswordChange:Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_change)


        init()
        registerListeners()
    }

    private fun init(){
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonPasswordChange = findViewById(R.id.buttonPasswordChange)
    }
    private fun registerListeners(){
        buttonPasswordChange.setOnClickListener(){

            val newPassword = editTextPassword.text.toString()

            if (newPassword.isEmpty() || newPassword.length < 6){
                Toast.makeText(this, "Password must be more than 5 symbols", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!newPassword.matches(".*[A-Z].*".toRegex())) {
                Toast.makeText(this, "Password must contains minimum 1 upper-case character", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!newPassword.matches(".*[a-z].*".toRegex())) {
                Toast.makeText(this, "Password must contains minimum 1 lower-case character", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!newPassword.matches(".*[0-9].*".toRegex())) {
                Toast.makeText(this, "Password must contains minimum 1 number", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                ?.addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        Toast.makeText(this, "Successful", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Task Unsuccessful", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }
}